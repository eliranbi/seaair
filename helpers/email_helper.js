var moment = require('moment');
var mongoose = require('mongoose');
var Owner = require('../models/owner');
var Email = require('../models/email');
var Parcel = require('../models/parcel');

//sending email ... 
var sendgrid  = require('sendgrid')("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");
var jade = require('jade');


module.exports = {

  sendParcelEmail: function(parcel, owner, done){

  		console.log(">> sendParcelEmail() - Apt:" + parcel.apt + " [" + owner.email + "]");

  		//set the parcel/package attribuites.
        var options = {
          name: owner.owner_name,
          package_provider: parcel.vendor,
          total_packages: parcel.total_packages,
          pacakge_referance: parcel.referance,
          apt : owner.apt,
          date: moment(new Date()).format('lll')
        }

		console.log(">> sendParcelEmail() - options:" + options);  		
        
        if(owner.email == ""){
        	done({success: false, msg:"No Email Address on File", error:{}});
        	console.log(">> No Email Address on File");
        	return;		 
        } 

		//send email ...
		var html = jade.renderFile('views/emails/package.jade', options);
        sendgrid.send({
          to:       owner.email,
          from:     'eliranbi@gmail.com',
          subject:  'SeaAirTowers - ' + owner.apt + ' - New Package',
          html:     html
        }, function(err, json) {
          if (err) { 
          	done({success: false, error: err}); //return the callback ...        
          }
          console.log(">> sendParcelEmail() -> Email sent ...  ");   
          //update sent time ...             		  
          console.log(json);		          
          done({success: true, email: owner.email}); //return the callback ...        
        });        
  },

  updateSentTime: function(objId){
	
  },

  sayHelloInEnglish: function() {
    return "HELLO";
  },
       
  sayHelloInSpanish: function() {
    return "Hola";
  }

};