var moment = require('moment');
var mongoose = require('mongoose');
var Owner = require('../models/owner');
var Email = require('../models/email');
var Parcel = require('../models/parcel');
var User = require('../models/user');


module.exports = {

  canDeleteUser: function(id, done){
  		console.log(">> canDeleteUser():" + id);
      //find how many user admin are ...
      var userIsAdmin = false;
      var totalAdmin = 0;
      var _canDeleteUser = true;
      User.find({isAdmin: true}, function(err,doc){
        console.log(doc);
        totalAdmin = doc.length;
        doc.forEach(function(user){
          if(user._id == id){
            console.log(">> canDeleteUser -> user._id == id")
            userIsAdmin = true;
          }
        });
        console.log(">> canDeleteUser -> totalAdmin = " + totalAdmin);
        if(userIsAdmin && totalAdmin==1) _canDeleteUser = false;
        done(_canDeleteUser);
      });  		
  }

};