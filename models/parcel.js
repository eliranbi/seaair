var mongoose = require('mongoose');
var Owner = require('../models/owner');
//var EmailHelper = require('../helpers/email_helper');

var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var parcelScheam = new Schema({
    apt: String,
    vendor: String,
    email: String,
    total_packages: Number,
    operator_id: ObjectId,
    owner_id: ObjectId,
    received_date: Date,
    create_date: Date,
    update_date: Date,
    picked_date: Date,
    referance: String,
	  attempts: Number,
    picked: Boolean,
    email_sent : Date
});


// on every save, add the date
//parcelScheam.pre('save', function(next, done) {
parcelScheam.pre('save', function(next) {
    console.log(">>parcelScheam.pre('save') ... ");
    // get the current date
    var currentDate = new Date();
    this.received_date = currentDate,
    this.create_date = currentDate,
    this.update_date = currentDate,
    this.picked = false    
    var self = this;
    if(this.attempts == undefined){
      this.attempts = 0;
    }else{
      this.attempts = this.attempts+1;
    }    
    next();

  /*
  //need to get the user email.
  if(this.email == "" || this.email == undefined){
    Owner.findOne({apt:this.apt}, function(err, doc){
      console.log(">>parcelScheam - Owner: " + doc);
      if (err || !doc) {
         next(err);
      }else{                    
        self.owner_id = doc._id;   
        self.email = doc.email; 
        console.log(">>parcelScheam - this.owner_id: " + self.owner_id);        
        //try to send email and save the sent time ... 
        
        //EmailHelper.sendParcelEmail(this, doc, function(rsp){
        //  console.log(">>> EmailHelper -> CallBack():" + rsp);
        //  self.email_sent = new Date();
        //  this.attempts = 1;     
        //  next();     
        //});                 
        
        next();
      }       
    });    
  }else{
    next();
  }
  */

});

parcelScheam.post('save', function(obj) {
    console.log(">> parcelScheam.post('save') ... ");
  // get the current date
  var currentDate = new Date();  
  //need to get the user email.
  var tmpParcel = this;
  Owner.findOne({apt:obj.apt}, function(err, doc){
      console.log(">> parcelScheam - post save - Owner: " + doc);
      if (err || !doc) {
        console.log(err);
      }else{            
        //this.apt = doc.apt;            
        console.log(">>parcelScheam - doc.owner_id: " + doc.owner_id);           
        console.log(">>parcelScheam - pack: ");
        console.log(obj);   
        // SEND EMAIL
        /*
        EmailHelper.sendParcelEmail(tmpParcel, doc, function(rsp){
          console.log(">>> EmailHelper -> CallBack():" + rsp);
          tmpParcel.email_sent = new Date();
          tmpParcel.save(function(err){});
          done();
        });         
        */        
      }       
    });  
});

// the schema is useless so far
// we need to create a model using it
var Parcel = mongoose.model('Parcel', parcelScheam);

// make this available to our users in our Node applications
module.exports = Parcel;