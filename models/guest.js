// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var guestScheam = new Schema({
            first_name: String,
            last_name: String,
			      full_name: String,
            apt: String,
            start_date: Date,
            end_date: Date,
            owner_id: String,
            email: String,
            phone: String,
            mobile: String,
            create_date: Date,
            'status': String,
            update_date: Date,
		        app_fee_paid: Boolean,
			      app_deposit_paid: Boolean,
			//new fields:
			comment:String,
			type: String,
			category: String, 
			insert_date: String
});

// on every save, add the date
guestScheam.pre('save', function(next) {
	console.log(">>guestScheam.pre('save') ... ");
  // get the current date
  var currentDate = new Date();
  
  // change the updated_at field to current date
  this.update_date = currentDate;
  this.owner_id = this.apt;

  // if created_at doesn't exist, add to that field
  if (!this.create_date){
  	this.create_date = currentDate;
  }
  // create full_name for search.  
  this.full_name = this.first_name + ' ' + this.last_name;
  
  next();
});

// the schema is useless so far
// we need to create a model using it
var Guest = mongoose.model('Guest', guestScheam);

// make this available to our users in our Node applications
module.exports = Guest;



/* Short way.
	var mongoose = require('mongoose');
	module.exports = mongoose.model('Guest',{
            first_name: String,
            last_name: String,
            apt: String,
            start_date: String,
            end_date: String,
            owner_id: Number,
            email: String,
            phone: String,
            mobile: String,
            create_date: Date,
            'status': String,
            update_date: Date
});

*/