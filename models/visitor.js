// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var visitorScheam = new Schema({
            name: String,
            comment: String,
			      mobile: String,
            apt: String,            
            owner_id: String,            
            create_date: Date,
            'status': String,
            type: String,
            category: String,
            update_date: Date
});

// on every save, add the date
visitorScheam.pre('save', function(next) {
	console.log(">>visitorScheam.pre('save') ... ");
  // get the current date
  var currentDate = new Date();
  
  // change the updated_at field to current date
  this.update_date = currentDate;
  this.owner_id = this.apt;
  this.status = "A";

  // if created_at doesn't exist, add to that field
  if (!this.create_date){
  	this.create_date = currentDate;
  }
  next();
});

// the schema is useless so far
// we need to create a model using it
var Visitor = mongoose.model('Visitor', visitorScheam);

// make this available to our users in our Node applications
module.exports = Visitor;
