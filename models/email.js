// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
ObjectId = Schema.ObjectId;
// create a schema
var emailScheam = new Schema({			      
            Apt: String,            
            Email: String,
            Name: String,            
            'status': String,
            create_date: Date,            
            update_date: Date,
            owner_id: ObjectId      			
});

// on every save, add the date
emailScheam.pre('save', function(next) {
	console.log(">>emailScheam.pre('save') ... ");
  // get the current date
  var currentDate = new Date();  
  // change the updated_at field to current date
  this.update_date = currentDate;
  // if created_at doesn't exist, add to that field
  if (!this.create_date){
  	this.create_date = currentDate;
  }
  next();
});

// the schema is useless so far
// we need to create a model using it
var Email = mongoose.model('Email', emailScheam);

// make this available to our users in our Node applications
module.exports = Email;
