// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var ownerScheam = new Schema({
			      owner_name: String,
            apt: String,
            address: String,
            mailing_address: String,            
            email: String,
            mobile: String,
            home: String,
            work: String,
            //phone: String,
            'status': String,
            create_date: Date,
            close_date: Date,            
            update_date: Date,
      			comment:String, 
            parking: String
});

// on every save, add the date
ownerScheam.pre('save', function(next) {
	console.log(">>ownerScheam.pre('save') ... ");
  // get the current date
  var currentDate = new Date();  
  // change the updated_at field to current date
  this.update_date = currentDate;
  this.owner_id = this.apt;
  // if created_at doesn't exist, add to that field
  if (!this.create_date){
  	this.create_date = currentDate;
  }
  next();
});

// the schema is useless so far
// we need to create a model using it
var Owner = mongoose.model('Owner', ownerScheam);

// make this available to our users in our Node applications
module.exports = Owner;
