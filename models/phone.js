// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var phoneScheam = new Schema({
            Apt: String,
            Name: String,
			      Home: String,
            Work: String,
            Mobile: String            
});

// on every save, add the date
phoneScheam.pre('save', function(next) {
	console.log(">>phoneScheam.pre('save') ... ");  
  next();
});

var Phone = mongoose.model('Phone', phoneScheam);
// make this available to our users in our Node applications
module.exports = Phone;
