var express = require('express');
var router = express.Router();
var moment = require('moment');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

var sendgrid  = require('sendgrid')("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");

router.get('/send', function(req, res, next) {
  //res.send('send email - respond with a resource');
  sendgrid.send({
				  //to:       'eliranbi@gmail.com',
				  to: 'manager@seaairtowers.org',
				  from:     'eliranbi@gmail.com',
				  subject:  'SeaAirTowers - PH14 - New Package',
				  text:     'You have a new package waiting for you in the mailroom !'
				}, function(err, json) {
				  if (err) { return console.error(err); }
				  console.log(json);
				  res.send('send email - respond with a resource');
				})
});

var jade = require('jade');
var options = {
  		name: "Yuda Gabay",
  		package_provider: "UPS",
  		pacakge_number: "10231",
  		apt : "PH14",
  		date: moment(new Date()).format('lll')
  	}
var html = jade.renderFile('views/emails/package.jade', options);
router.get('/send1', function(req, res, next) {
  //res.send('send email - respond with a resource');
  sendgrid.send({
				  to:       'manager@seaairtowers.org',
				  from:     'eliranbi@gmail.com',
				  subject:  'SeaAirTowers - PH14 - New Package',
				  html:     html
				}, function(err, json) {
				  if (err) { return console.error(err); }
				  console.log(json);
				  res.send('send email - respond with a resource');
				})
});

/*

var payload   = {
  to      : 'to@example.com',
  from    : 'from@other.com',
  subject : 'Saying Hi',
  text    : 'This is my first email through SendGrid'
}

sendgrid.send(payload, function(err, json) {
  if (err) { console.error(err); }
  console.log(json);
});

*/


router.get('/view', function(req, res, next) {
  //res.send('send email - respond with a resource');
  res.render('emails/package',{
  		name: "Yuda Gabay",
  		package_provider: "UPS",
  		pacakge_number: "10231",
  		apt : "PH14",
  		date: moment(new Date()).format('lll')
  	})
});



router.get('/marge', function(req, res, next) {
  res.send('>> marge  ...');
  var db = req.db;
  var collection = db.get('guests');
  collection.find({},{},function(e,guestList){
	  if(e){
		console.log(">> error:" + e)
		  return e;
	  }
	  for (var i in guestList) {
	    tmp = guestList[i];
	    console.log(tmp);
		
		//create new guest ...
		var guest = new Guest();
		guest.first_name = tmp.first_name.toLowerCase();
	    guest.last_name = tmp.last_name.toLowerCase();
	    guest.apt = tmp.apt;
	    guest.start_date = new Date(tmp.start_date);
	    guest.end_date = new Date(tmp.end_date);
	    guest.owner_id = tmp.apt;
	    guest.email = '';
	    guest.phone = tmp.phone;
	    guest.mobile = tmp.phone
		guest.status = 'A';
		guest.app_fee_paid = false ;
		guest.app_deposit_paid = false ;
		guest.comment = tmp.comment
		guest.type = tmp.type
		guest.category = tmp.category
		try{
			guest.insert_date = new Date(tmp.insert_date)
		}catch(e){
			guest.insert_date = new Date();
			console.error(">>error: " + e);
		}
				
        // save the user
        guest.save(function(err) {
            if (err){
                console.log('Error in Saving guest : '+err);  
                throw err;  
            }
            console.log('New guest was added succesfuly');    
	        res.redirect("home");
        });	
		
		
		
	  }
  });
});


module.exports = router;
