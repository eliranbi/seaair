
var express = require('express');
var router = express.Router();
var Guest = require('../models/guest');
var Phone = require('../models/phone');
var Owner = require('../models/owner');

/* GET users listing. */
router.get('/', function(req, res, next) {
  console.log(">> in / ..." );
  var localDB = req.localDB;
  var collection = localDB.get('phones');  
  collection.find({},{},function(e,list){
	console.log(">> list : "  + list.length);
	  if(e){
	  	console.log(">> error:" + e);
	  	return e;
	  }
	  list.forEach(function(tmp){
	  	//var tmp = list[i];
	  	console.log(">> ph:" + JSON.stringify(tmp) + "\n\r");
	  	Owner.findOne({ apt: tmp.Apt}, function(err, eOwner) {
            if (err){
                console.log(err);
            }
            if (eOwner) {
                eOwner.mobile = tmp.Mobile;
                eOwner.work = tmp.Work;
                eOwner.home = tmp.Home;
                eOwner.save();
                console.log(eOwner.apt + " -> " + eOwner.mobile + " >>> Saved !");
        	}
		});
	  });
	  //for(var i in list){	  	
	  //}
  	  res.send(list);
  });
  
});



router.get('/marge', function(req, res, next) {
  res.send('>> marge  ...');
  var db = req.db;
  var collection = db.get('guests');
  collection.find({},{},function(e,guestList){
	  if(e){
		console.log(">> error:" + e)
		  return e;
	  }
	  for (var i in guestList) {
	    tmp = guestList[i];
	    console.log(tmp);
		
		//create new guest ...
		var guest = new Guest();
		guest.first_name = tmp.first_name.toLowerCase();
	    guest.last_name = tmp.last_name.toLowerCase();
	    guest.apt = tmp.apt;
	    guest.start_date = new Date(tmp.start_date);
	    guest.end_date = new Date(tmp.end_date);
	    guest.owner_id = tmp.apt;
	    guest.email = '';
	    guest.phone = tmp.phone;
	    guest.mobile = tmp.phone
		guest.status = 'A';
		guest.app_fee_paid = false ;
		guest.app_deposit_paid = false ;
		guest.comment = tmp.comment
		guest.type = tmp.type
		guest.category = tmp.category
		try{
			guest.insert_date = new Date(tmp.insert_date)
		}catch(e){
			guest.insert_date = new Date();
			console.error(">>error: " + e);
		}
				
        // save the user
        guest.save(function(err) {
            if (err){
                console.log('Error in Saving guest : '+err);  
                throw err;  
            }
            console.log('New guest was added succesfuly');    
	        res.redirect("home");
        });	
		
		
		
	  }
  });
});


router.get('/add_phones', function(req, res, next) {
  res.send('>> add_phones');
  var db = req.db;
  var collection = db.get('guests');
  collection.find({},{},function(e,guestList){
	  if(e){
		console.log(">> error:" + e)
		  return e;
	  }
	  for (var i in guestList) {
	    tmp = guestList[i];
	    console.log(tmp);
		
		//create new guest ...
		var guest = new Guest();
		guest.first_name = tmp.first_name.toLowerCase();
	    guest.last_name = tmp.last_name.toLowerCase();
	    guest.apt = tmp.apt;
	    guest.start_date = new Date(tmp.start_date);
	    guest.end_date = new Date(tmp.end_date);
	    guest.owner_id = tmp.apt;
	    guest.email = '';
	    guest.phone = tmp.phone;
	    guest.mobile = tmp.phone
		guest.status = 'A';
		guest.app_fee_paid = false ;
		guest.app_deposit_paid = false ;
		guest.comment = tmp.comment
		guest.type = tmp.type
		guest.category = tmp.category
		try{
			guest.insert_date = new Date(tmp.insert_date)
		}catch(e){
			guest.insert_date = new Date();
			console.error(">>error: " + e);
		}
				
        // save the user
        guest.save(function(err) {
            if (err){
                console.log('Error in Saving guest : '+err);  
                throw err;  
            }
            console.log('New guest was added succesfuly');    
	        res.redirect("home");
        });	
		
		
		
	  }
  });
});

router.get('/update_email', function(req, res, next) {  
  res.send('>> update_email');	
  console.log(">> update_email -> in update owner emails ...");
  var localDB = req.localDB;
  var collection = localDB.get('emails');
  collection.find({},{},function(e,email_list){
  	  console.log(">> update_email -> found email list ...");
  	  console.log(">> update_email -> found email Total:" + email_list.length);
  	  
	  if(e){
		  console.log(">> error finding emails ... :" + e)
		  return e;
	  }
	  email_list.forEach(function(tmp){
	    console.log(">> [" + tmp.Apt + "] >>> :" + tmp.Email);
	    
	    	Owner.findOne({apt:tmp.Apt.toString()}, function(err, doc){
      		console.log(">> update_email -> Owner[" + tmp.Apt + "] ");
      		if (err || !doc) {
        		console.error(">> update_email -> NO APT FOUND : " + tmp.Apt);
      		}else{            
        		//this.apt = doc.apt;
        		doc.email = tmp.Email;
        		doc.save(function(err){
        			if(err){
        				console.log(">> update_email -> err: " + err);
        			}
        			console.log(">> Update email : " + tmp.Email + " - for : " + doc.apt);
        		})
      		}       
    	});      	
						
	  }); //end for each list ... 

  });
});


module.exports = router;
