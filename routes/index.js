var express = require('express');
var router = express.Router();
var Guest = require('../models/guest');
var Visitor = require('../models/visitor');
var Owner = require('../models/owner');
var Parcel = require('../models/parcel');
var User = require('../models/user');
var mongoose = require('mongoose');
var moment = require('moment');
var EmailHelper = require('../helpers/email_helper');
var ModelHelper = require('../helpers/models_helper');

/*
app.use(function(req, res, next) {
    res.locals = {
        isAuthenticated: req.isAuthenticated(),
        path :req.path
    }

    next(); // make sure we go to the next routes and don't stop here
});
*/

var isAuthenticated = function (req, res, next) {
	// if user is authenticated in the session, call the next() to call the next request handler 
	// Passport adds this method to request object. A middleware is allowed to add properties to
	// request and response objects
	if (req.isAuthenticated())
		return next();
	// if the user is not authenticated then redirect him to the login page
	res.redirect('/');
}

var isAuthenticatedAdmin = function (req, res, next) {
	// if user is authenticated in the session, call the next() to call the next request handler 
	// Passport adds this method to request object. A middleware is allowed to add properties to
	// request and response objects
	if (req.isAuthenticated()){
		console.log(">> user:" + req.user);
		if(req.user.isAdmin){
			return next();
		}else{
			req.flash('message', 'User Not Autorized.');
		}			
	}
		
	// if the user is not authenticated then redirect him to the login page
	res.redirect('notautorized');
}


module.exports = function(passport){
	router.get('/getguestbyparam', function (req, res) {
		//var guest = req.params.search_guest;	
		//var guest = req.body.search_guest;
		//ajax come as query param.			
		var guest = req.query.search_guest;
		console.log('>> getguestbyparam -  guest:' + guest);			
		Guest.find(
			//{ $or:[ {'full_name':guest}, {'apt':guest} ]}, 
			//{ $or: [{full_name: new RegExp(guest, 'i')}, {apt: guest}] }, function(err, guests) {
				{ $or: [{full_name: new RegExp(guest, 'i')}, {apt: new RegExp(guest, 'i')}] }, function(err, guests) {
            // In case of any error, return using the done method
            if (err){
                console.log('Error in SignUp: '+err);
                return done(err);
            }
            // already exists
            if (guests) {
                console.log('>> found guests:' + guests);				
		        //res.render('guests', {
		        //    "reslist" : guests
				//});				
        	}else{
        		console.log('>> guests not fount guests!');
        	}
			//send the query response to ajax:
			var rsp = {guests: guests, isAdmin: req.user.isAdmin}
			//res.send(guests);
			res.send(rsp);
		}); //.sort('first_name');		
	});

	
	router.get('/getvisitorbyparam', function (req, res) {
		var visitor = req.query.search_visitor;
		console.log('>> getvisitorbyparam - visitor:' + visitor);			
		Visitor.find({ $or: [{name: new RegExp(visitor, 'i')}, {apt: new RegExp(visitor, 'i')}] }, 
			function(err, visitors) {
	            if (err){
	                console.log('Error in SignUp: '+err);
	                return done(err);
	            }            
	            if (visitors) {// visitors exists
	                console.log('>> found visitors:' + visitors);				
	        	}else{
	        		console.log('>> visitor not found in visitors!');
	        	}
				//send the query response to ajax:
				var rsp = {visitors: visitors, isAdmin: req.user.isAdmin}
				//res.send(guests);
				res.send(rsp);
		}); //.sort('first_name');		
	});


	router.get('/getownerbyparam', function (req, res) {
		var owner = req.query.search_owner;
		console.log('>>getownerbyparam -  owner:' + owner);			
		Owner.find({ $or: [{owner_name: new RegExp(owner, 'i')}, {apt: new RegExp(owner, 'i')}] }, 
			function(err, owners) {
	            if (err){
	                console.log('Error in SignUp: '+err);
	                return done(err);
	            }            
	            if (owners) {// visitors exists
	                console.log('>> found owners:' + owners.length); // + owners);				
	        	}else{
	        		console.log('>> owner not found in owners!');
	        	}
				//send the query response to ajax:
				var rsp = {owners: owners, isAdmin: req.user.isAdmin}
				//res.send(guests);
				res.send(rsp);
		}); //.sort('first_name');		
	});

	router.get('/getparcelbyparam', function (req, res) {
		var parcel = req.query.search_parcel;
		console.log('>>getparcelbyparam -  parcel:' + parcel);			
		Parcel.find({ $or: [
							{referance: new RegExp(parcel, 'i')}, 
							{apt: new RegExp(parcel, 'i')}
						   ] 
					}, function(err, parcels) {
	            if (err){
	                console.log('Error in getparcelbyparam: '+err);
	                return done(err);
	            }            
	            if (parcels) {// visitors exists
	                console.log('>> found owners:' + parcels.length); // + owners);				
	        	}else{
	        		console.log('>> owner not found in parcels!');
	        	}
				//send the query response to ajax:
				var rsp = {parcels: parcels, isAdmin: req.user.isAdmin}
				//res.send(guests);
				res.send(rsp);
		}); //.sort('first_name');		
	});

	router.get('/mark_parcel_picked', function (req, res) {
		var parcelId = req.query._id;
		var now = new Date();
		console.log(">> mark_parcel_picked - parcelId:" + parcelId);
		Parcel.findOneAndUpdate(
			{ _id: parcelId },
			{ '$set': { 
						picked: true, 
						picked_date: now
					} },
			 function(err, doc) {
	            if (err){
	                console.log('Error in mark_parcel_picked: '+err);
	                return done(err);
	            }
	            if (doc) {	            	
	                console.log('>> found and update :' + doc);	
	                res.send({
	                	"parcel": doc,
	                	"picked" : true
	                });			
	        	}else{
	        		console.log('>> not found and update!' + doc);
	        		res.send({
	                	"parcel": doc,
	                	"picked" : false
	                });			
	        	}							
		});
	});

	router.get('/parcel_resend_email', function (req, res) {
		var parcelId = req.query._id;
		var now = new Date();
		var _parcel = new Parcel();
		console.log(">> parcel_resend_email - parcelId:" + parcelId);
		Parcel.findById(parcelId, function(err, doc){
			if(err){
				    res.send({"parcel": doc, "sent" : false});			
			}else{
				_parcel =  doc;
				//Need to get Owner name and email ...
				Owner.findOne({apt:doc.apt}, function(err, owner){
					if(err){
				    	res.send({
	                		"parcel": {},
	                		"sent" : false
	                	});				
				    }else{
						// send email ...   				     
						EmailHelper.sendParcelEmail(doc, owner, function(rsp){
							console.log(">>> parcel_resend_email - EmailHelper -> CallBack():" + rsp.success);
							//Save the parcel ... 
							if(rsp.success){
								_parcel.email_sent = new Date();
								_parcel.attempts = _parcel.attempts + 1;					  	
								_parcel.save(function(err) {
									console.log(">> parcel_resend_email : email send and saved ...");
									if (!err){
										res.send({"parcel": _parcel,"sent" : true });				
									}
								}); //end of save ... 	
							}else{
								res.send({"parcel": _parcel,"sent" : false });				
							}								
						}); // end of email send ..
					}//end of else 		
				}); 
			}//end of else ...
		});		
	});
	

	router.get('/edit_visitor', function (req, res) {
		var visitorId = req.query.id;
		console.log(">> edit_visitor [id]:" + visitorId);
		Visitor.findOne({ _id: visitorId}, function(err, eVisitor) {
            if (err){
                console.log('Error in SignUp: '+err);
                return done(err);
            }
            if (eVisitor) {
                console.log('>> found guest:' + eVisitor);				
        	}else{
        		console.log('>> guests not fount visitor!');
        	}			
			res.render('edit_visitor', 
				{ "visitor": eVisitor }
			);
		});
	});
		
	router.post('/edit_visitor', function(req, res) {		
		Visitor.findById(req.body._id, function(err, uVisitor) {
		  if (!uVisitor){
		    return next(new Error('Could not load Document'));
		  }else {
		    // do your updates here
			uVisitor.first_name = req.body.name;
			uVisitor.apt = req.body.apt;
	  	    uVisitor.comment = req.body.comment;
	  	    uVisitor.category = req.body.category;
	  	    uVisitor.owner_id = req.body.apt;	  	    	  	    	  	    
	  		uVisitor.status = 'A';
			uVisitor.update_date = new Date();			
		    uVisitor.save(function(err) {
		      if (err)
				  console.log('>> error update guest ...: ' + err);
		      else
				  console.log('>> success update guest ...');
			  res.redirect('visitors');	  
		    });
		  }
		});				
	});
	
	/*router.get('/guest_details/:guestid', function (req, res) {
		var guestId = req.params.guestid;
	});*/

	router.get('/edit_guest', function (req, res) {
		var guestId = req.query.id;
		console.log(">> edit_guest [id]:" + guestId);
		Guest.findOne({ _id: guestId}, function(err, eGuest) {
            if (err){
                console.log('Error in SignUp: '+err);
                return done(err);
            }
            if (eGuest) {
                console.log('>> found guest:' + eGuest);				
        	}else{
        		console.log('>> guests not fount guest!');
        	}			
			res.render('edit_guest', 
				{ "guest": eGuest }
			);
		});
	});
		

	router.post('/edit_guest', function(req, res) {		
		Guest.findById(req.body._id, function(err, uGuest) {
		  if (!uGuest){
		    return next(new Error('Could not load Document'));
		  }else {
		    // do your updates here
			uGuest.first_name = req.body.first_name;
			uGuest.last_name = req.body.last_name;
			uGuest.email = req.body.email;
			uGuest.phone = req.body.phone
			uGuest.mobile = req.body.mobile;
			uGuest.start_date = req.body.start_date;
			uGuest.end_date = req.body.end_date;
			uGuest.apt = req.body.apt;
	  	    uGuest.comment = req.body.comment;
	  	    uGuest.type = req.body.type;
	  	    uGuest.category = req.body.category;
	  	    uGuest.owner_id = req.body.apt;	  	    	  	    	  	    
	  		uGuest.status = 'A';
			uGuest.update_date = new Date();
			uGuest.app_fee_paid = req.body.app_fee_paid;
            uGuest.app_deposit_paid = req.body.app_deposit_paid;
		    uGuest.save(function(err) {
		      if (err)
				  console.log('>> error update guest ...: ' + err);
		      else
				  console.log('>> success update guest ...');
			  res.redirect('home');	  
		    });
		  }
		});				
	});
	

	//- Owner edit ... 

	router.get('/edit_owner', function (req, res) {
		var ownerId = req.query.id;
		console.log(">> edit_owner [id]:" + ownerId);
		Owner.findOne({ _id: ownerId}, function(err, eOwner) {
            if (err){
                console.log('Error in SignUp: '+err);
                return done(err);
            }
            if (eOwner) {
                console.log('>> found guest:' + eOwner);				
        	}else{
        		console.log('>> guests not fount guest!');
        	}			
			res.render('edit_owner', 
				{ "owner": eOwner }
			);
		});
	});

	router.post('/edit_owner', function(req, res) {		
		Owner.findById(req.body._id, function(err, uOwner) {
		  if (!uOwner){
		    return next(new Error('Could not load Document'));
		  }else {
		    // do your updates here
			uOwner.owner_name = req.body.owner_name;			
			uOwner.email = req.body.email;
			uOwner.home = req.body.home
			uOwner.mobile = req.body.mobile;
			uOwner.work = req.body.work;
			uOwner.mailing_address = req.body.mailing_address
			uOwner.close_date = req.body.close_date;			
			uOwner.apt = req.body.apt;	 
			uOwner.parking = req.body.parking;	  	    
			uOwner.update_date = new Date();			
		    uOwner.save(function(err) {
		      if (err)
				  console.log('>> error update guest ...: ' + err);
		      else
				  console.log('>> success update guest ...');
			  res.redirect('owners');	  
		    });
		  }
		});				
	});


	router.get('/delete_guest', function(req, res, next) {		
		Guest.findById(req.query.id, function(err, dGuest) {
		  if (!dGuest){
		    return next(new Error('Could not load Document'));
		  }else {
		    // do your updates here
		    dGuest.remove(function(err) {
		      if (err)
		        console.log('>> error: ' + err)
		      else
				  console.log('>> success user _id:[' + dGuest._id + '] - deleted!');
			  res.redirect('home');	  
		    });
		  }
		});				
	});	

	router.get('/delete_user', function(req, res) {
		console.log(">> delete_user : " + req.query.user_id);
		var rsp = {users: [], isAdmin: req.user.isAdmin, canDelete: true};
		User.findById(req.query.user_id, function(err, dUser) {
		  if (!dUser){
		    res.send(rsp);
		  }else {	
		  	ModelHelper.canDeleteUser(req.query.user_id, function(canDelete){
		  		console.log(">> delete_user: canDelete :" + canDelete);		  		
		  		if(canDelete){
		  			dUser.remove(function(err) {
				      if (!err) console.log('>> success user _id:[' + dUser._id + '] - deleted!');		        
					  //insert find users ...
					  User.find({},function(err, users) {            
					  	console.log(">> find total :" + users.length);	            
			            if (!err){	                
			            	rsp = {users: users, isAdmin: req.user.isAdmin, canDelete: true}	
			            }		
			            console.log(">> delete_user - rsp : " + rsp);
						res.send(rsp);
					  }); 
				    });		
		  		}else{
		  			res.send({users:[], canDelete: false, isAdmin: req.user.isAdmin});
		  		}
		  		
		  	});

		    
		  } //end of else ..
		});				
	});	
	
		
	router.get('/delete_visitor', function(req, res, next) {		
		Visitor.findById(req.query.id, function(err, dVisitor) {
		  if (!dVisitor){
		    return next(new Error('Could not load Document'));
		  }else {
		    // do your updates here
		    dVisitor.remove(function(err) {
		      if (err)
		        console.log('>> error: ' + err)
		      else
				  console.log('>> success visitor _id:[' + dVisitor._id + '] - deleted!');
			  res.redirect('visitors');	  
		    });
		  }
		});				
	});			

	/* GET login page. */
	router.get('/', function(req, res) {
    	// Display the Login page with any flash message, if any
		res.render('login', { message: req.flash('message') });
	});

	/* Handle Login POST */
	router.post('/login', passport.authenticate('login', {
		successRedirect: '/home',
		failureRedirect: '/',
		failureFlash : true  
	}));

	// GET main page
	router.get('/main', isAuthenticated, function(req, res){
		res.render('main', { 
			user: req.user, 
			title: 'Main Page'
		});
	});

	// GET main page
	router.get('/details', isAuthenticated, function(req, res){		
		var guestId = req.query.id;
		console.log(">> edit_guest [id]:" + guestId);
				Guest.findOne({ _id: guestId}, function(err, eGuest) {
		            if (err){
		                console.log('Error in SignUp: '+err);
		                return done(err);
		            }
		            if (eGuest) {
		                console.log('>> found guest:' + eGuest);				
		        	}else{
		        		console.log('>> guests not fount guest!');
		        	}			
					
					Owner.findOne({ apt: eGuest.apt}, function(err, eOnwer) {
			            if (err){
			                console.log('Error in SignUp: '+err);
			                return done(err);
			            }

						res.render('details', { 
									"guest": eGuest ,
									"owner": eOnwer,
									title: 'Guest Details'
									});
					});			
				});
	});

	router.get('/owner_details', isAuthenticated, function(req, res){		
		var ownerId = req.query.id;
		Owner.findOne({ _id: ownerId}, function(err, eOnwer) {
            if (err){
                console.log('Error in SignUp: '+err);
                return done(err);
            }
			res.render('owner_details', { "owner": eOnwer, title: 'Owner Details'});
		});			
	});

	router.get('/notautorized', function(req, res){
		res.render('notautorized', { 
			title: 'Not Autorized!'
		});
	});
	
	router.get('/add_visitor', isAuthenticated, function(req, res) {
	    res.render('add_visitor', { title: 'Add New Visitor' });
	});

	// POST to Add User Service 
	router.post('/add_visitor', function(req, res) {
		var visitor = new Visitor();
		visitor.name = req.body.name;
	    visitor.apt = req.body.apt;	    
		visitor.comment = req.body.comment;
		visitor.type = req.body.type;
		visitor.category = req.body.category;
		console.log(">> visitor:" + visitor);
	    
        // save the visitor
        visitor.save(function(err) {
            if (err){
			    // If it failed, return error
			    //res.send("There was a problem adding the information to the database.");
                console.log('>> Error in Saving guest : '+err);  
                throw err;  
            }
            console.log('>> New visitor was added succesfuly');    
	        res.redirect("visitors");
        });		
	});

	router.get('/add_parcel', function(req, res) {
	    res.render('add_parcel', { title: 'Add New Parcel' });
	});		
	
	/*
	app.get('/document/:id', function(req, res) {
	  Document.findOne({ _id : req.params.id }, function(error, document) {
	    if (error || !document) {
	      res.render('error', {});
	    } else {
	      res.render('document', { document : document });
	    }
	  });
	});

	*/

	// POST to Add User Service 
	router.post('/add_parcel', function(req, res) {
		var parcel = new Parcel();
	    var _id = req.body._id;	    
		parcel.apt = req.body.apt;
	    parcel.referance = req.body.referance_number;
	    parcel.vendor = req.body.vendor;
	    parcel.total_packages = req.body.total_packages;	    
	    parcel.comment = req.body.comment;
	    if(parcel.referance == undefined || parcel.referance == "" ){
      		parcel.referance == (new Date()).getTime();
    	}
		console.log(">> parcel:" + parcel);				
		//find the user email ...
		Owner.findOne({apt:req.body.apt}, function(err, doc){      
        	console.log(">>parcelScheam - Owner: " + doc);
        	if (err || !doc) {
        		console.log(">> error :" + err);
	      		res.render("add_parcel", {message: "Apt #" + req.body.apt + " not found, please try again!"});
	    	} else {	      		
	      		parcel.apt = doc.apt;
            	parcel.owner_id = doc._id; 
            	if(doc.email != undefined && doc.email != ""){
            		parcel.email = doc.email;
            	}
            	
            	// send email ...        
		        EmailHelper.sendParcelEmail(this, doc, function(rsp){
		          console.log(">>> EmailHelper -> CallBack():" + rsp.success);
				  //Save the parcel ... 
				  	if(rsp.success){
				  		parcel.email_sent = new Date();
				  		parcel.attempts = 1;					  	
				  	}
				  	
		        	parcel.save(function(err) {
			            if (err){
						    // If it failed, return error
						    //res.send("There was a problem adding the information to the database.");
			                console.log('>> Error in Saving parcel : '+err);  
			                throw err;  
			            }
			            console.log('>> New parcel was added succesfuly');    
			        	res.redirect("parcels");
		        	});		
		        });                                    	
	    	}        		        			
    	}); 
        
	});

	router.get('/contact', function(req, res) {
	    res.render('contact', { title: 'Contact Us' });
	});

	router.get('/add_guest', isAuthenticatedAdmin, function(req, res) {
	    res.render('add_guest', { title: 'Add New Guest' });
	});		
	
	// POST to Add User Service 
	router.post('/add_guest', function(req, res) {

		var guest = new Guest();
	    var userName = req.body.username;
	    var userEmail = req.body.useremail;
		var _id = req.body._id;	    

		guest.first_name = req.body.first_name;
	    guest.last_name = req.body.last_name;
	    guest.apt = req.body.apt;
	    guest.start_date = req.body.start_date;
	    guest.end_date = req.body.end_date;;
	    guest.owner_id = req.body.apt;
	    guest.email = req.body.email;
	    guest.phone = req.body.phone
	    guest.mobile = req.body.mobile;
		guest.status = 'A';
		guest.app_fee_paid = (req.body.app_fee_paid) ? true : false ;
		guest.app_deposit_paid = (req.body.app_deposit_paid) ? true : false ;
		guest.comment = req.body.comment;
		guest.type = req.body.type;
		guest.category = req.body.category;
		console.log(">> guest:" + guest);
		
        // save the guest
        guest.save(function(err) {
            if (err){
			    // If it failed, return error
			    //res.send("There was a problem adding the information to the database.");
                console.log('>> Error in Saving guest : '+err);  
                throw err;  
            }
            console.log('>> New guest was added succesfuly');    
	        res.redirect("home");
        });		
	});
	

	/* GET Registration Page */
	/*
	router.get('/signup', isAuthenticatedAdmin, function(req, res){
		res.render('register',{message: req.flash('message')});
	});
	*/
	router.get('/signup', function(req, res){
		res.render('register',{message: req.flash('message')});
	});


	/* Handle Registration POST */
	router.post('/signup', passport.authenticate('signup', {
		successRedirect: '/home',
		failureRedirect: '/signup',
		failureFlash : true  
	}));

	/* GET Home Page 
	router.get('/home', isAuthenticated, function(req, res){
		res.render('home', { user: req.user });
	});
	*/
	/* Handle Logout */
	router.get('/signout', function(req, res) {
		req.logout();
		res.redirect('/');
	});
	

	router.get('/visitors', isAuthenticated, function(req, res) {	
        Visitor.find( {}, function(err, visitors) {
            // In case of any error, return using the done method
            if (err){
                console.log('Error in visitors: '+err);
                return done(err);
            }
            var total = visitors.length;
	        res.render('visitors', {
	            "visitorlist" : visitors,
	            "total_guests": total, 
	            "title" : "Visitors List"
	        });
            
        });
	});
	
	router.get('/owners', isAuthenticated, function(req, res) {	 
        Owner.find( {}, function(err, owners) {
            if (err){
                console.log('Error in owners: '+err);
                return done(err);
            }
            var total = owners.length;
	        res.render('owners', {
	            "ownerlist" : owners,
	            "total_owners": total, 
	            "title" : "Owners List"
	        });            
        });
	});


	router.get('/users', isAuthenticated, function(req, res) {	 
       	User.find( {}, function(err, users) {
            if (err){
                console.log('Error in users: '+err);
                return done(err);
            }
            var total = users.length;
	        res.render('users', {
	            "userlist" : users,
	            "total_users": total, 
	            "title" : "Users List"
	        });            
        });
	});

	router.get('/parcels', isAuthenticated, function(req, res) {	 
        Parcel.find( {}, function(err, parcels) {
            if (err){
                console.log('Error in parcels: '+err);
                return done(err);
            }
            var total = parcels.length;
	        res.render('parcels', {
	            "parcellist" : parcels,
	            "total_parcels": total, 
	            "title" : "Parcels List"
	        });            
			
        });
	});

	router.get('/home', isAuthenticated, function(req, res) {	 
        var today = moment().startOf('day')
		var start = moment(today).subtract(2, 'days')
		var end = moment(today).add(3, 'days')		
		console.log(">> Date range : [" + moment(start).format("L") +"  - " + moment(end).format("L") +"]");

		Guest.find({
			start_date: { 
							$gte: start.toDate(), 
							$lt: end.toDate()
						}  			
  				}, function(err, guests){
  					if (err){
                console.log('Error in home: '+err);
                return done(err);
            }
            var total = guests.length;
	        res.render('guests', {
	            "reslist" : guests,
	            "total_guests": total, 
	            "title" : "Guest List"
	        });            
  		}).sort('start_date');		

		/*

        Guest.find( {}, function(err, guests) {
            // In case of any error, return using the done method
            if (err){
                console.log('Error in home: '+err);
                return done(err);
            }
            var total = guests.length;
	        res.render('guests', {
	            "reslist" : guests,
	            "total_guests": total, 
	            "title" : "Guest List"
	        });            
        });*/
	});	

	//return the router
	return router;
}

        /**
		db.getCollection('guests').find( 
			{
				$query:{ 
							start_date: { 
											$gte: ISODate('2015-11-23'), 
											$lt: ISODate('2015-11-30') 
										}
						}, $orderby: { start_date : -1 }
			})
	 	*/   
		/*
		var db = req.db;
	    var collection = db.get('reservations');
	    collection.find({},{},function(e,docs){
	        res.render('reservations', {
	            "reslist" : docs
	        });
	    });
		*/

