$(function(){
 //var isAdmin = !{isAdmin};	
 $('#search_guest').on('keyup', function(e){
   if(e.keyCode === 13) {
     var parameters = { search_guest: $(this).val() };
       $.get( '/getguestbyparam',parameters, function(data) {
       //$('#results').html(data);
		   buildGuestList(data);
     });
    };
 });

 $('#search_by_btn').on('click', function(e){
     var parameters = { search_guest: $('#search_guest').val() };
       $.get( '/getguestbyparam',parameters, function(data) {
       //$('#results').html(data);
		   buildGuestList(data);
     });
 });

 function buildGuestList(data){
	 var resList = data.guests;
	 var isAdmin = data.isAdmin;
	 $('#guestListContainer').html('');
	 var html = "";
	 for(var i=0; i<resList.length;i++){
		 if(isAdmin){
			 html += "<div class='row rowList'>";
			 html += '<div class="col-xs-1 coList">';
			 html += '<a href="/edit_guest?id=' + resList[i]._id + '">' + resList[i].apt + '</a></div>';
			 html += '<div class="col-xs-2 coList">' + resList[i].full_name + '</div>';
			 html += '<div class="col-xs-3 coList">' + resList[i].mobile + ' / ' +  resList[i].phone +  '</div>';
			 html += '<div class="col-xs-1 coList">' + resList[i].status + '</div>';
			 html += '<div class="col-xs-2 coList">' + resList[i].email + '</div>';
			 html += '<div class="col-xs-1 coList">' + resList[i].start_date + '</div>';
			 html += '<div class="col-xs-1 coList">' + resList[i].end_date + '</div>';
			 html += "</div>";		 	
		 				
		 }else{
			 html += "<div class='row rowList'>";
			 html += '<div class="col-sm-1 coList">' + resList[i].apt + '</a></div>';
			 //html += '<a href="/edit_guest?id=' + resList[i]._id + '">' + resList[i].apt + '</a></div>';
			 html += '<div class="col-sm-2 coList">' + resList[i].full_name + '</div>';
			 html += '<div class="col-xs-3 coList">' + resList[i].mobile + ' / ' + resList[i].phone + '</div>';
			 html += '<div class="col-sm-2 coList">' + resList[i].start_date + '</div>';
			 html += '<div class="col-sm-2 coList">' + resList[i].end_date + '</div>';
			 html += "</div>";		 	
		 }
	 }
	 $('#guestListContainer').html(html);	 	 
 }


});


