$(function(){
 //var isAdmin = !{isAdmin};	
 $('#search_guest').on('keyup', function(e){
   if(e.keyCode === 13) {
     var parameters = { search_guest: $(this).val() };
       $.get( '/getguestbyparam',parameters, function(data) {       
		   buildGuestList(data);
     });
    };
 });

 $('#search_by_btn').on('click', function(e){
     var parameters = { search_guest: $('#search_guest').val() };
       $.get( '/getguestbyparam',parameters, function(data) {
		   buildGuestList(data);
     });
 });

 function buildGuestList(data){
	 var resList = data.guests;
	 var isAdmin = data.isAdmin;
	 $('#guestListContainer').html('');
	 var html = "<table class='table table-bordered'>"
	 html += getTableHead(isAdmin);
	 html += "<tbody>";		
	 for(var i=0; i<resList.length;i++){
		html += '<tr>';
		if(isAdmin){
			html += '<td><a href="/edit_guest?id=' + resList[i]._id + '">' + resList[i].apt + '</a></td>';
		}else{
			html += '<td>' + resList[i].apt + '</td>';
		}
		html += '<td>' + resList[i].full_name + '</td>';
		html += '<td>' + resList[i].mobile + '</td>';		
		html += '<td>' + moment(resList[i].start_date).format('L') + '</td>';
		html += '<td>' + moment(resList[i].end_date).format('L') + '</td>';
		if(isAdmin){
			html += '<td>' + getGuestType(resList[i].type) + '</td>';
			html += '<td>' + getGuestCategory(resList[i].category) + '</td>';			
		}
		
		html += '</tr>';		 			 				
	 }
	 html+="</tbody></table>";
	 $('#guestListContainer').html(html);	 	 
 }

 function getTableHead(isAdmin){
	 var html = '<thead>';
     html += '<tr>';
     html += '<th class="thGuest">Apt #</th>';
     html += '<th class="thGuest">Full Name</th>';
     html += '<th class="thGuest">Mobile/Other</th>';	 
     html += '<th class="thGuest">Start Date</th>';
     html += '<th class="thGuest">End Date</th>';	
	 if(isAdmin){
	 	html += '<th class="thGuest">Type</th>';
	 	html += '<th class="thGuest">Relation</th>';
	 } 
	 html += '</tr></tbody>'
	 return html;
 }

 function boolValue(val){
	 if(val == undefined || val == null){
	 	return 'No'
	 }
	 else if(val=='' || val == false || val == 'false'){
		return 'No';		 
	 }else if(val == 'true' || val == true){
		return 'Yes';
	 }else{
		return 'No';
	 }
 }	

 function getGuestCategory(st){	
		var cat = "";
		switch(st) {
			case 'FS':
				cat = "Spouse";
				break;
			case 'FP':
				cat = "Parent";
				break;
			case 'FR':
				cat = "Relative";
				break;
			case 'FC':
				cat = "Child";            
				break;
			case 'CT':
				cat = "Care Taker";    
				break;
			case 'V':
				cat = "Visitor";            
				break;
			case 'O':
				cat = "Owner";        
				break;
			default:
				cat = st;
				break;			
		}
		return cat;	
	}			

 function getGuestType(st){	
		var type = "";
		switch(st) {
			case 'T':
				type = "Seasonal";
				break;
			case 'P':
				type = "Permanent";
				break;
			case 'R':
				type = "Renter";
				break;			
			default:
				type = "Unknown";
				break;			
		}
		return type;	
	}		

});


