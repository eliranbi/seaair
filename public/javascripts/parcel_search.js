$(function(){

 //var isAdmin = !{isAdmin};	
 
 $('#search_parcel').on('keyup', function(e){
   if(e.keyCode === 13) {
     var parameters = { search_parcel: $(this).val() };
       $.get( '/getparcelbyparam',parameters, function(data) {       
		   buildParcelList(data);
     });
    };
 });

  $('#search_parcel_by_btn').on('click', function(e){
     var parameters = { search_parcel: $('#search_parcel').val() };
       $.get( '/getparcelbyparam',parameters, function(data) {       
		   buildParcelList(data);
     });
 });

 $('.parcel_picked').on('click', function(e){
     var parameters = { _id: $( this ).attr("_id") };
       $.get( '/mark_parcel_picked',parameters, function(data) {
		   //TODO
		   console.log(data);
		   if(data.picked){
		   	bootbox.alert("Package ref # [" + data.parcel.referance + "] - marked as picked!", function() {
  				//Example.show("Hello world callback");
			});
		   }
     });
 });

$('.parcel_resend_email').on('click', function(e){
     var parameters = { _id: $( this ).attr("_id") };
       $.get( '/parcel_resend_email',parameters, function(data) {
		   //TODO
		   console.log(data);
		   if(data.sent){
		   	bootbox.alert("Email was sent to [" + data.parcel.email + "]", function() {
  				//Example.show("Hello world callback");
			});
		   }else{
		   	bootbox.alert("Please make sure Email Address is on File!", function() {
  				//Example.show("Hello world callback");
			});
		   }
     });
 });


 function buildParcelList(data){
	 var parcelList = data.parcels;
	 var isAdmin = data.isAdmin;
	 $('#parcelListContainer').html('');
	 var html = "<table class='table table-bordered'>"
	 html += getTableHead(isAdmin);
	 html += "<tbody>";		
	 for(var i=0; i<parcelList.length;i++){
		html += '<tr>';
		if(isAdmin){
			//html += '<td><a href="/edit_owner?id=' + parcelList[i]._id + '">' + parcelList[i].apt + '</a></td>';
			html += '<td>' + parcelList[i].apt + '</td>';
		}else{
			html += '<td>' + parcelList[i].apt + '</td>';
		}
		html += '<td>' + parcelList[i].referance + '</td>';
		html += '<td>' + moment(parcelList[i].received_date).format('L') + '</td>';		
		html += '<td>' + parcelList[i].vendor + '</td>';					
		//html += '<td>' + parcelList[i].attempts + '</td>';
		if(parcelList[i].picked){
			html += '<td style="text-align:center;"><i class="icon fa fa-check text-green"></i></td>';
		}else{
			html += '<td style="text-align:center;"><i class="icon fa fa-ban text-red"></i></td>';
		}
		//html += '<td>' + parcelList[i].picked + '</td>';		
		html += '<td>' + getDateTime(parcelList[i].email_sent) + '</td>';			
		html += '<td>' + getDateTime(parcelList[i].picked_date) + '</td>';							
		html += '<td>' ;
		html += '<div class="btn-group">';
	    html += '<button type="button" class="btn btn-info">Action</button>';
	    html += '<button type="button" data-toggle="dropdown" aria-expanded="false" class="btn btn-info dropdown-toggle"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>';
	    html += '<ul role="menu" class="dropdown-menu">';
	    html += '<li><a href="#" class="parcel_picked" _id="' + parcelList[i]._id + '">Picked</a></li>';
	    html += '<li><a href="#" class="parcel_resend_email" _id="' + parcelList[i]._id + '">Resend Email</a></li>';
	    html += '</ul>';
	    html += '</div>';
	    html += '</td>';							

		html += '</tr>';		 			 				
	 }
	 html+="</tbody></table>";
	 $('#parcelListContainer').html(html);	 	 
 }

 function getTableHead(isAdmin){
	 var html = '<thead>';
     html += '<tr>';
     html += '<th class="thGuest">Apt #</th>';
     html += '<th class="thGuest">Ref #</th>';
     html += '<th class="thGuest">Recevied</th>';	 
     html += '<th class="thGuest">Vendor</th>';
     //html += '<th class="thGuest">Attempts</th>';
     html += '<th class="thGuest">Picked</th>';
     html += '<th class="thGuest">Email Sent</th>';
     html += '<th class="thGuest">Picked Date</th>';
     html += '<th class="thGuest">Action</th>';
	 html += '</tr></tbody>';
	 return html;
 }

 function getDateTime(date){
 		if(date == undefined) return "--/--/--";
		var nDate = moment(date).format('L') + ", " + moment(date).format('h:mm A');	
		return nDate
 }

 function boolValue(val){
	 if(val == undefined || val == null){
	 	return 'No'
	 }else if(val=='' || val == false || val == 'false'){
		return 'No';		 
	 }else if(val == 'true' || val == true){
		return 'Yes';
	 }else{
		return 'No';
	 }
 }	

});