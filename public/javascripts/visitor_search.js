$(function(){
 //var isAdmin = !{isAdmin};	
 $('#search_visitor').on('keyup', function(e){
   if(e.keyCode === 13) {
     var parameters = { search_visitor: $(this).val() };
       $.get( '/getvisitorbyparam',parameters, function(data) {       
		   buildVisitorList(data);
     });
    };
 });

 $('#search_visitor_by_btn').on('click', function(e){
     var parameters = { search_visitor: $('#search_visitor').val() };
       $.get( '/getvisitorbyparam',parameters, function(data) {
		   buildVisitorList(data);
     });
 });

$('#search_visitor_by_btn').on('tap', function(e){
     var parameters = { search_visitor: $('#search_visitor').val() };
       $.get( '/getvisitorbyparam',parameters, function(data) {
		   buildVisitorList(data);
     });
 });

 function buildVisitorList(data){
	 var visitorList = data.visitors;
	 var isAdmin = data.isAdmin;
	 $('#visitorListContainer').html('');
	 var html = "<table class='table table-bordered'>"
	 html += getTableHead(isAdmin);
	 html += "<tbody>";		
	 for(var i=0; i<visitorList.length;i++){
		html += '<tr>';
		/*
		if(isAdmin){
			html += '<td><a href="/edit_visitor?id=' + visitorList[i]._id + '">' + visitorList[i].apt + '</a></td>';
		}else{
			html += '<td>' + visitorList[i].apt + '</td>';
		}*/
		html += '<td><a href="/edit_visitor?id=' + visitorList[i]._id + '">' + visitorList[i].apt + '</a></td>';
		html += '<td>' + visitorList[i].name + '</td>';
		html += '<td>' + getGuestCategory(visitorList[i].category) + '</td>';		
		html += '<td>' + visitorList[i].comment + '</td>';			
		html += '</tr>';		 			 				
	 }
	 html+="</tbody></table>";
	 $('#visitorListContainer').html(html);	 	 
 }

 function getTableHead(isAdmin){
	 var html = '<thead>';
     html += '<tr>';
     html += '<th class="thGuest">Apt #</th>';
     html += '<th class="thGuest">Visitor Name</th>';
     html += '<th class="thGuest">Relation</th>';	 
     html += '<th class="thGuest">Comment</th>';     
	 html += '</tr></tbody>'
	 return html;
 }

 function boolValue(val){
	 if(val == undefined || val == null){
	 	return 'No'
	 }else if(val=='' || val == false || val == 'false'){
		return 'No';		 
	 }else if(val == 'true' || val == true){
		return 'Yes';
	 }else{
		return 'No';
	 }
 }	

 function getGuestCategory(st){	
		var cat = "";
		switch(st) {
			case 'FS':
				cat = "Spouse";
				break;
			case 'FP':
				cat = "Parent";
				break;
			case 'FR':
				cat = "Relative";
				break;
			case 'FC':
				cat = "Child";            
				break;
			case 'CT':
				cat = "Care Taker";    
				break;
			case 'V':
				cat = "Visitor";            
				break;
			case 'O':
				cat = "Owner";        
				break;
			default:
				cat = st;
				break;			
		}
		return cat;	
	}			

 function getGuestType(st){	
		var type = "";
		switch(st) {
			case 'T':
				type = "Seasonal";
				break;
			case 'P':
				type = "Permanent";
				break;
			case 'R':
				type = "Renter";
				break;			
			default:
				type = "Unknown";
				break;			
		}
		return type;	
	}		

});


