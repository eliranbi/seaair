$(function(){
	
 $('#ownerDelete').on('click', function(e){
     var id = $('#_id').val();
	 var name = $('#owner_name').val();
	 bootbox.confirm("Are you sure you want to delete owner: " + name + " ?", function(result) {
	   console.log("Confirm result: "+result);
	   if(result){
	   		window.location.href='/delete_owner?id=' + id;
	   }
	 }); 
 });

 $('#ownerCancel').on('click', function(e){
	 window.location.href='/owners';
 });

});


