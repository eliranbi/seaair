$(function(){
 //var isAdmin = !{isAdmin};	
 $('#search_owner').on('keyup', function(e){
   if(e.keyCode === 13) {
     var parameters = { search_owner: $(this).val() };
       $.get( '/getownerbyparam',parameters, function(data) {       
		   buildOwnerList(data);
     });
    };
 });

 $('#search_owner_by_btn').on('click', function(e){
     var parameters = { search_owner: $('#search_owner').val() };
       $.get( '/getownerbyparam',parameters, function(data) {
		   buildOwnerList(data);
     });
 });

 function buildOwnerList(data){
	 var ownerList = data.owners;
	 var isAdmin = data.isAdmin;
	 $('#ownerListContainer').html('');
	 var html = "<table class='table table-bordered'>"
	 html += getTableHead(isAdmin);
	 html += "<tbody>";		
	 for(var i=0; i<ownerList.length;i++){
		html += '<tr>';
		if(isAdmin){
			html += '<td><a href="/edit_owner?id=' + ownerList[i]._id + '">' + ownerList[i].apt + '</a></td>';
		}else{
			html += '<td>' + ownerList[i].apt + '</td>';
		}
		html += '<td>' + ownerList[i].owner_name + '</td>';
		html += '<td>' + ownerList[i].mobile + '</td>';		
		html += '<td>' + ownerList[i].home + '</td>';		
		//html += '<td>' + ownerList[i].address + '</td>';			
		if(isAdmin){
			html += '<td>' + ownerList[i].mailing_address + '</td>';
			html += '<td>' + ownerList[i].close_date + '</td>';		
			html += '<td>' + ownerList[i].email + '</td>';			
		}
		html += '</tr>';		 			 				
	 }
	 html+="</tbody></table>";
	 $('#ownerListContainer').html(html);	 	 
 }

 function getTableHead(isAdmin){
	 var html = '<thead>';
     html += '<tr>';
     html += '<th class="thGuest">Apt #</th>';
     html += '<th class="thGuest">Full Name</th>';
     html += '<th class="thGuest">Phone #</th>';	 
     html += '<th class="thGuest">Adress</th>';
     if(isAdmin){
     	html += '<th class="thGuest">Mailing Address</th>';     
     	html += '<th class="thGuest">Close Date</th>';     
     	html += '<th class="thGuest">Email</th>';          	
     }          
	 html += '</tr></tbody>'
	 return html;
 }

 function boolValue(val){
	 if(val == undefined || val == null){
	 	return 'No'
	 }else if(val=='' || val == false || val == 'false'){
		return 'No';		 
	 }else if(val == 'true' || val == true){
		return 'Yes';
	 }else{
		return 'No';
	 }
 }	

 function getGuestCategory(st){	
		var cat = "";
		switch(st) {
			case 'FS':
				cat = "Spouse";
				break;
			case 'FP':
				cat = "Parent";
				break;
			case 'FR':
				cat = "Relative";
				break;
			case 'FC':
				cat = "Child";            
				break;
			case 'CT':
				cat = "Care Taker";    
				break;
			case 'V':
				cat = "Visitor";            
				break;
			case 'O':
				cat = "Owner";        
				break;
			default:
				cat = st;
				break;			
		}
		return cat;	
	}			

 function getGuestType(st){	
		var type = "";
		switch(st) {
			case 'T':
				type = "Seasonal";
				break;
			case 'P':
				type = "Permanent";
				break;
			case 'R':
				type = "Renter";
				break;			
			default:
				type = "Unknown";
				break;			
		}
		return type;	
	}		

});


