$(function(){
 //var isAdmin = !{isAdmin};	
 $('#search_user').on('keyup', function(e){
   return;
   if(e.keyCode === 13) {
     var parameters = { search_user: $(this).val() };
       $.get( '/getuserbyparam',parameters, function(data) {       
		   buildUserList(data);
     });
    };
 });

 $('#search_user_by_btn').on('click', function(e){
 	return;
    var parameters = { search_user: $('#search_user').val() };
       $.get( '/getuserbyparam',parameters, function(data) {
		   buildUserList(data);
    });
 });

$('.userDeleteClass').on('click', function(e){
	 console.log(">>> this.attributes._id:" + this.getAttribute("_id"));
     var parameters = {user_id: this.getAttribute("_id")};
       $.get( '/delete_user', parameters, function(data) {
       	   if(data.canDelete != undefined && data.canDelete){
       	   	buildUserList(data);	
       	   }else{
       	   	bootbox.alert("User canno't be deleted!, please make sure there is at least one admin!", function() {  				
			});
       	   }
		   
     });
 });

 function buildUserList(data){
	 var userList = data.users;
	 var isAdmin = data.isAdmin;
	 $('#userListContainer').html('');
	 var html = "<table class='table table-bordered'>"
	 html += getTableHead(isAdmin);
	 html += "<tbody>";		
	 for(var i=0; i<userList.length;i++){
		html += '<tr>';		
		html += '<td>' + userList[i].username+ '</td>';
		html += '<td>' + userList[i].name + '</td>';
		html += '<td>' + userList[i].email + '</td>';		
		html += '<td>' + boolValue(userList[i].isAdmin) + '</td>';			
		html += '<td><button type="button" class="btn btn-block btn-danger userDeleteClass" _id="' + userList[i]._id+ '">Delete</button></td>';			
		html += '</tr>';		 			 				
	 }
	 html+="</tbody></table>";
	 $('#userListContainer').html(html);	 	 
 }

 function getTableHead(isAdmin){
	 var html = '<thead>';
     html += '<tr>';
     html += '<th class="thGuest">Username</th>';
     html += '<th class="thGuest">Name</th>';
     html += '<th class="thGuest">Email</th>';	 
     html += '<th class="thGuest">Admin</th>';     
     html += '<th class="thGuest">Action</th>';     
	 html += '</tr></tbody>'
	 return html;
 }

 function boolValue(val){
	 if(val == undefined || val == null){
	 	return 'NO'
	 }else if(val=='' || val == false || val == 'false'){
		return 'NO';		 
	 }else if(val == 'true' || val == true){
		return 'YES';
	 }else{
		return 'NO';
	 }
 }			
});


