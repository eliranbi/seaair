$(function(){
	
 $('#guestDelete').on('click', function(e){
     var id = $('#_id').val();
	 var name = $('#first_name').val() + ' ' + $('#last_name').val();
	 bootbox.confirm("Are you sure you want to delete guest: " + name + " ?", function(result) {
	   console.log("Confirm result: "+result);
	   if(result){
	   		window.location.href='/delete_guest?id=' + id;
	   }
	 }); 
 });

 $('#guestCancel').on('click', function(e){
	 window.location.href='/home';
 });

 $('#type').val(_type);
 $('#category').val(_category);

});


