$(function(){
 //var isAdmin = !{isAdmin};	
 $('#search_guest').on('keyup', function(e){
   if(e.keyCode === 13) {
     var parameters = { search_guest: $(this).val() };
       $.get( '/getguestbyparam',parameters, function(data) {
       //$('#results').html(data);
		   buildGuestList(data);
     });
    };
 });

 $('#search_by_btn').on('click', function(e){
     var parameters = { search_guest: $('#search_guest').val() };
       $.get( '/getguestbyparam',parameters, function(data) {
       //$('#results').html(data);
		   buildGuestList(data);
     });
 });

 function buildGuestList(data){
	 var resList = data.guests;
	 var isAdmin = data.isAdmin;
	 $('#guestListContainer').html('');
	 var html = "<table class='table table-bordered'>"
	 html += getTableHead(isAdmin);
	 html += "<tbody>";		
	 for(var i=0; i<resList.length;i++){
		html += '<tr>';
		if(isAdmin){
			html += '<td><a href="/edit_guest?id=' + resList[i]._id + '">' + resList[i].apt + '</a></td>';
		}else{
			html += '<td>' + resList[i].apt + '</a></td>';
		}
		html += '<td>' + resList[i].full_name + '</td>';
		html += '<td>' + resList[i].mobile + ' / ' +  resList[i].phone +  '</td>';
		if(isAdmin){
			html += '<td>' + resList[i].status + '</td>';
			html += '<td>' + boolValue(resList[i].app_fee_paid) + ' / ' + boolValue(resList[i].app_deposit_paid) + '</td>';
			html += '<td>' + resList[i].email + '</td>';
		}
		html += '<td>' + resList[i].start_date + '</td>';
		html += '<td>' + resList[i].end_date + '</td>';
		if(isAdmin)	html += '<td>' + resList[i].comment + '</td>';
		html += '</tr>';		 			 				
	 }
	 html+="</tbody></table>";
	 $('#guestListContainer').html(html);	 	 
 }

 function getTableHead(isAdmin){
	 var html = '<thead>';
     html += '<tr>';
     html += '<th class="thGuest">Apt #</th>';
     html += '<th class="thGuest">Full Name</th>';
     html += '<th class="thGuest">Mobile/Other</th>';
	 if(isAdmin){
     	html += '<th class="thGuest">Status</th>';
		html += '<th class="thGuest">Fee / Deposit </th>';
		html += '<th class="thGuest">Email</th>';
	 }
     html += '<th class="thGuest">Start Date</th>';
     html += '<th class="thGuest">End Date</th>';	
	 if(isAdmin) html += '<th class="thGuest">Comment</th>';
	 html += '</tr></tbody>'
	 return html;
 }

 function boolValue(val){
	 if(val == undefined || val == null){
	 	return 'No'
	 }
	 else if(val=='' || val == false || val == 'false'){
		return 'No';		 
	 }else if(val == 'true' || val == true){
		return 'Yes';
	 }else{
		return 'No';
	 }
 }		

});


