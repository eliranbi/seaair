$(function(){
 
 console.log(">> loading edit_visitor.js");
 $('#visitorDelete').on('click', function(e){
 	console.log(">> visitorDelete cliecked ... ");
     var id = $('#_id').val();
	 var name = $('#name').val();
	 bootbox.confirm("Are you sure you want to delete visitor: " + name + " ?", function(result) {
	   console.log("Confirm result: "+result);
	   if(result){
	   		window.location.href='/delete_visitor?id=' + id;
	   }
	 }); 
 });

 $('#visitorCancel').on('click', function(e){
	 window.location.href='/visitors';
 });

 $('#category').val(_category);
 $("#comment").text(_comment);

});


