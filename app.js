var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

// setting the required database connection.
var mongo = require('mongodb');
var monk = require('monk');
var db = monk('localhost:27017/mydb');
var localDB = monk('localhost:27017/seaair_db');

var dbConfig = require('./db.js');
var mongoose = require('mongoose');
var mongoose_import = require('mongoose');
mongoose.connect(dbConfig.url);
//mongoose_import.connect(dbConfig.url_import);


var routes = require('./routes/index');
var util = require('./routes/util');
var email = require('./routes/email');
//var users = require('./routes/users');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//create connection to the database.

// Make our db accessible to our router
app.use(function(req,res,next){
    req.db = db;
    req.localDB = localDB;
    next();
});

// Configuring Passport
var passport = require('passport');
var expressSession = require('express-session');
//app.use(expressSession({secret: 'mySecretKey'}));
app.use(expressSession({secret: 'seaa1rt0w3r', saveUninitialized: true, resave: true}));
app.use(passport.initialize());
app.use(passport.session());

app.get('*', function(req, res, next) {

  // just use boolean for loggedIn
  res.locals.loggedIn = (req.user) ? true : false;
  res.locals.isAdmin = false;
  res.locals.username = "";
  if(req.user){
    res.locals.user = req.user
	  res.locals.username = req.user.username;
	  var admin = req.user.isAdmin;
	  if(admin == null || admin == undefined || admin =='' || admin == false){
		  res.locals.isAdmin = false;
	  }else if (admin == true){
	  	  res.locals.isAdmin = true;
	  }
  }
  console.log(">>  [" + res.locals.username + "] res.locals.isAdmin: " +   res.locals.isAdmin);
  res.locals.path = req.path;
  console.log(req.path);
  next();
  
  /*	
  //make sure all requests are autenticated !	
  if(req.path == '/' || req.path == '/login'){
  	next();
  }else{
	  if(req.isAuthenticated()){
		  next();
	  }else{
		  //next(new Error(401)); // 401 Not Authorized
		  res.redirect('/');
	  }
  }  
  //next();
  */
});



// Initialize Passport
var initPassport = require('./passport/init');
initPassport(passport);

 // Using the flash middleware provided by connect-flash to store messages in session
 // and displaying in templates
var flash = require('connect-flash');
app.use(flash());

var routes = require('./routes/index')(passport);
app.use('/', routes);
app.use('/util', util);
app.use('/email', email);
//app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  //next(err);
  res.render('404.jade');
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
